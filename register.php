<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>My twitter</title>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="jumbotron jumbotron-sub">
		<nav class="navbar">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.html">Home</a></li>
            <li><a href="about.html">About</a></li>
            <li><a href="login.html">Login</a></li>
            <li class="active"><a href="register.html">Register</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

		<div class="container">
		<h1>lRegister</h1>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="alert" id="msg"></div>
				<form id="regForm">
				  <div class="form-group">
				    <label>First Name</label>
				    <input type="text" class="form-control" id="name" name="name">
				  </div>
				  <div class="form-group">
				    <label>Email</label>
				    <input type="text" class="form-control" id="email" name="email">
				  </div>
				  <div class="form-group">
				    <label>Password</label>
				    <input type="password" class="form-control" id="password" name="password">
				  </div>
				  <div class="form-group">
				    <label>Confirm Password</label>
				    <input type="password" class="form-control" id="password2" name="password2">
				  </div>
				  <button type="submit" class="btn btn-default">Register</button>
				</form>
			</div>
		</div>
	</div>
</div>

	<footer class="main-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
				<h3></h3>
				<p></p>
				<p>Copyright &copy; 2016</p>
				</div>
				
			</div>
		</div>
	</footer>

	<script   src="https://code.jquery.com/jquery-1.12.4.js"   integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="   crossorigin="anonymous"></script>
	<script>
		function register(e){
			e.preventDefault();
			
			var name = document.getElementById('name').value;
			var email = document.getElementById('email').value;
			var password = document.getElementById('password').value;
			var password2 = document.getElementById('password2').value;
			var msg = document.getElementById('msg');

			if(name == '' || email == '' || password == '' || password2 == ''){
				msg.className="alert alert-danger";
				msg.innerHTML = 'Please fill out all fields';
			} else {
				if(name.length < 3){
					msg.className="alert alert-danger";
					msg.innerHTML = 'Name must be at least 3 characters';
				} else {
					var atpos = email.indexOf("@");
					var dotpos = email.lastIndexOf(".");
					if(atpos < 1 || dotpos < atpos+2 || dotpos+2 >= email.length){
						msg.className="alert alert-danger";
						msg.innerHTML = 'Please use valid email';
					} else {
						if(password != password2){
							msg.className="alert alert-danger";
							msg.innerHTML = 'Passwords do not match';
						} else {
							// Success
							msg.className="alert alert-success";
							msg.innerHTML = name+' is registered with email '+ email;
						}
					}
				}
			}		
		}

		document.getElementById('regForm').addEventListener('submit', register, false);
	</script>
    <script src="js/bootstrap.js"></script>
</body>
</html>