<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>My twitter</title>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="jumbotron jumbotron-main">
		<nav class="navbar">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!-- <a class="navbar-brand" href="#">Strapped</a> -->
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="index.html">Home</a></li>
            <li><a href="about.html">About</a></li>
            <li><a href="login.html">Login</a></li>
            <li><a href="register.html">Register</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

		<div class="container">
		<h1>My twitter</h1>
		<p class="lead">This is my twitter web application project, you can login, register, add tweets and you can follow friends just like normal twitter, you know</p>
		<p><a class="btn btn-primary btn-lg" href="about.html">Read More</a> </p>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-12 box">
				<div class="well">
					<span class="glyphicon glyphicon-home"></span>
					<h3>Twitter App Development</h3>
					<p>This is my twitter web application project, you can login, register, add tweets and you can follow friends just like normal twitter, you know</p>
				</div>
			</div>
		</div>
	</div>

	<footer class="main-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
				<p> </p>
				<p>Copyright &copy; 2016</p>
				</div>
			</div>
		</div>
	</footer>

	<script   src="https://code.jquery.com/jquery-1.12.4.js"   integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="   crossorigin="anonymous"></script>
    <script src="js/bootstrap.js"></script>
</body>
</html>